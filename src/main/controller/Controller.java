package controller;

import entity.Carrinho;
import entity.Cliente;
import entity.Produto;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class Controller {
    Carrinho carrinho = new Carrinho();
    List<Cliente> clienteList = new ArrayList<>();
    public List<Cliente> cadastrarCliente (Cliente cliente) {

        if(cliente.getId() == null || cliente.getNome() == null){
            System.out.println("Erro, precisa inserir as informações corretas");
        }
        else{
            clienteList.add(cliente);
            System.out.println("Cadastrado com sucesso!!");
        }
        return clienteList;

    }

    public Carrinho carrinhoCompra (Produto produto) {
        if(clienteList.isEmpty()){
            System.out.println("Faça seu cadastro antes de fazer suas compras!");

        }else {
            Calendar c = Calendar.getInstance();
            carrinho.setCliente(clienteList.get(0));
            carrinho.setProduto(produto);
            carrinho.setDataCompra(c.getTime());
        }
        return carrinho;
    }

    public int idAutomatico () {
       int i = 0;
       int valor = i + 1;
       if(clienteList.size() > 0){
           valor += clienteList.get(0).getId();
       }
       return valor;
    }


    public String relatorio() {
        String relatorio = "";
        if(carrinho.getProduto() == null || carrinho.getCliente() == null){
            System.out.println("Não é possível gerar relatório sem dados");
        }else {
            relatorio = "Cliente: "+ carrinho.getCliente().getNome() + ";"
                    + " Produto: " + carrinho.getProduto().getNome() + ";" + " Data da compra: "
                    + carrinho.getDataCompra();
        }
        return relatorio;
    }
}
