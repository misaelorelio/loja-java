import controller.Controller;
import entity.Cliente;
import entity.Produto;

import java.util.Scanner;

public class Programa {
    public static void main(String[] args) {

        Controller controller = new Controller();

        Scanner sc = new Scanner(System.in);
        Cliente cliente = new Cliente();
        Produto produto = new Produto();
        var ciclo = 0;
        while (ciclo == 0) {
            System.out.println("--Menu escolha sua opção----------");
            System.out.println("--Opção 1 - Cadastro de cliente -- ");
            System.out.println("--Opção 2 - Cadastro de produto -- ");
            System.out.println("--Opção 3 - Gerar relatório------- ");
            System.out.println("Informe uma opção: ");
            ciclo = sc.nextInt();
            if(ciclo == 1){
                System.out.println("Nome do cliente: ");
                cliente.setNome(sc.next());
                cliente.setId(controller.idAutomatico());
                controller.cadastrarCliente(cliente);
                ciclo = 0;
            } else if (ciclo == 2) {
                System.out.println("Nome do produto: ");
                produto.setNome(sc.next());
                produto.setId(controller.idAutomatico());
                System.out.println("Valor R$: ");
                produto.setValor(sc.nextDouble());
                controller.carrinhoCompra(produto);
                ciclo = 0;
            } else if (ciclo == 3) {
                System.out.println(controller.relatorio());
                ciclo = 0;
            }
        }
    }
}