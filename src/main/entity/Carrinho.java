package entity;

import java.util.Calendar;
import java.util.Date;

public class Carrinho {
    private Date dataCompra;
    private Produto produto;
    private Cliente cliente;

    public Carrinho(Date dataCompra, Produto produto, Cliente cliente) {
        this.dataCompra = dataCompra;
        this.produto = produto;
        this.cliente = cliente;
    }

    public Carrinho() {

    }

    public Date getDataCompra() {
        return dataCompra;
    }

    public void setDataCompra(Date dataCompra) {
        this.dataCompra = dataCompra;
    }

    public Produto getProduto() {
        return produto;
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }
}
