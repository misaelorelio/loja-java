package entity;

import java.util.ArrayList;
import java.util.List;

public class Cliente {

    private String nome;
    private Integer id;

    public Cliente(String nome, Integer id) {
        this.nome = nome;
        this.id = id;
    }

    public Cliente() {

    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

}
