package entity;
public class Produto {

    private String nome;
    private Integer id;
    private Double valor;

    public Produto(String nome, Integer id, Double valor) {
        this.nome = nome;
        this.id = id;
        this.valor = valor;
    }

    public Produto() {

    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }
}